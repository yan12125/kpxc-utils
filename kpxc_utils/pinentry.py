import os
import sys

from kpxc_utils.backend import Kpxc, KpxcException

# Roughly follows https://github.com/djherbis/keepass-pinentry/blob/master/KeepassPinentry/Pinentry.cs
# assuan protocol responses: https://www.gnupg.org/documentation/manuals/assuan/Server-responses.html
# Vanilla impl: https://github.com/gpg/pinentry/blob/master/pinentry/pinentry.c

class KpxcPinentry:
    # See /usr/include/gpg-error.h
    GPG_ERR_SOURCE_PINENTRY = 5
    GPG_ERR_INTERNAL = 63
    GPG_ERR_NOT_IMPLEMENTED = 69
    GPG_ERR_CANCELED = 99
    GPG_ERR_NO_PASSPHRASE = 177

    def __init__(self):
        self.kpxc = Kpxc()
        self._send_line('OK Pleased to meet you')
        self.keygrip = None

    @staticmethod
    def _send_line(line: str) -> None:
        print(line, flush=True)

    @classmethod
    def _send_data(cls, data: str) -> None:
        cls._send_line(f'D {data}')

    @classmethod
    def _send_ok(cls) -> None:
        cls._send_line('OK')

    @classmethod
    def _send_err(cls, code: int, msg: str) -> None:
        err_code = cls.GPG_ERR_SOURCE_PINENTRY*(2**24)+code
        # Seems gpg does not show error strings from pinentry, but whatever...
        cls._send_line(f'ERR {err_code} {msg}')

    @classmethod
    def _default_handler(cls, argument: str):
        cls._send_err(cls.GPG_ERR_NOT_IMPLEMENTED, 'Not implemented')

    def handle_getpin(self, argument: str) -> None:
        if not self.keygrip:
            self._send_err(self.GPG_ERR_NO_PASSPHRASE, 'No keygrip given')
            return

        try:
            url = f'gpg://{self.keygrip}'
            self._send_data(self.kpxc.get_credentials(url)[0]['password'])
            self._send_ok()
        except KpxcException as ex:
            self._send_err(self.GPG_ERR_NO_PASSPHRASE, str(ex))

    def handle_option(self, argument: str) -> None:
        self._send_ok()

    def handle_setdesc(self, argument: str) -> None:
        self._send_ok()

    def handle_setprompt(self, argument: str) -> None:
        self._send_ok()

    def handle_getinfo(self, argument: str) -> None:
        if argument == 'pid':
            self._send_data(str(os.getpid()))
            self._send_ok()
        else:
            self._send_err(self.GPG_ERR_NOT_IMPLEMENTED, 'Not implemented')

    def handle_setkeyinfo(self, argument: str) -> None:
        # The char before / indicates the cache mode (Normal, Ssh or User) - seems not useful here
        # https://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob;f=agent/call-pinentry.c;h=45ec1b58e5bc9b88330c01c92c681b012d183b2c;hb=refs/heads/master#l1499
        if argument == '--clear':
            self.keygrip = None
        else:
            self.keygrip = argument.split('/')[1]
        self._send_ok()

    def pinentry_loop(self) -> None:
        while True:
            line = sys.stdin.readline().strip('\n')
            if not line:
                break
            parts = line.split(' ', maxsplit=2)
            cmd = parts[0]
            argument = None if len(parts) == 1 else parts[1]
            if cmd == 'BYE':
                break
            handler = getattr(self, 'handle_' + cmd.lower(), self._default_handler)
            handler(argument)

def main():
    KpxcPinentry().pinentry_loop()

if __name__ == '__main__':
    main()
