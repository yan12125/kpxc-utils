import sys

from kpxc_utils.backend import Kpxc

# Mimic pass(1)
# https://man.archlinux.org/man/pass.1
class Pass:
    def __init__(self):
        self.kpxc = Kpxc()

    def handle(self, argv):
        if not argv:
            return False

        action = argv[0]
        return getattr(self, f'handle_{action}')(argv[1:])

    def handle_show(self, argv):
        pass_name = argv[0]
        print(self.kpxc.get_credentials(pass_name)[0]['password'])
        return True

def main():
    pass_impl = Pass()
    argv = sys.argv[1:]
    sys.exit(1 if not pass_impl.handle(argv) else 0)

if __name__ == '__main__':
    main()
