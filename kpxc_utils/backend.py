import os
import pathlib

import platformdirs
from keepassxc_browser import Connection, Identity, ProtocolError
from keepassxc_browser.protocol import DEFAULT_SOCKET_NAME

class KpxcException(Exception):
    def __init__(self, msg=None):
        self.msg = msg

    def __str__(self):
        msg = self.msg
        if not msg and hasattr(self, '__cause__'):
            msg = str(self.__cause__)
        return msg

class Kpxc:
    def __init__(self):
        self.kpxc_conn = None

    def _connect_kpxc(self):
        if self.kpxc_conn:
            return

        client_identity = 'python-keepassxc-browser'

        data_dir = pathlib.Path(platformdirs.user_data_dir(client_identity, appauthor=False, roaming=True))
        data_dir.mkdir(parents=True, exist_ok=True)
        state_file = data_dir / 'state'
        if state_file.exists():
            with state_file.open('r') as f:
                data = f.read()
            self.identity = Identity.unserialize(client_identity, data)
        else:
            self.identity = Identity(client_identity)

        pinentry_user_data = os.getenv('PINENTRY_USER_DATA')
        if pinentry_user_data == 'remote':
            self.kpxc_conn = Connection(socket_name=DEFAULT_SOCKET_NAME + '-remote')
        else:
            self.kpxc_conn = Connection(socket_name=DEFAULT_SOCKET_NAME)
        self.kpxc_conn.connect()
        self.kpxc_conn.change_public_keys(self.identity)
        try:
            self.kpxc_conn.get_database_hash(self.identity)
        except ProtocolError:
            self.kpxc_conn.wait_for_unlock()

        if not self.kpxc_conn.test_associate(self.identity):
            assert self.kpxc_conn.associate(self.identity)
            data = self.identity.serialize()
            with state_file.open('w') as f:
                f.write(data)
            state_file.chmod(0o600)
            del data

    def get_credentials(self, url):
        try:
            self._connect_kpxc()
            if not self.kpxc_conn:
                raise KpxcException('Cannot connect to KeePassXC')
            return self.kpxc_conn.get_logins(self.identity, url=url)
        except ProtocolError as ex:
            raise KpxcException from ex
        finally:
            pass

    def __del__(self):
        if self.kpxc_conn:
            self.kpxc_conn.disconnect()
